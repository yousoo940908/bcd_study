import { message } from "."


export class Tran {
    
    public static async get(url:string, params:string){
        const axiosReturn = await message.tran('get',url, params)
        return axiosReturn
    }

    public static async post(url:string, params:{}){
        const axiosReturn = await message.tran('post',url, params)
        return axiosReturn
    }
}