package com.kakaobank.bcd.mapper;

import java.util.ArrayList;

import org.apache.ibatis.annotations.Mapper;

import com.kakaobank.bcd.domain.EmpnoDto;

@Mapper
public interface EmpnoMapper {	
	EmpnoDto selectbyEmpno(String empno);
}
