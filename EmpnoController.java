package com.kakaobank.bcd.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.kakaobank.bcd.domain.EmpnoDto;
import com.kakaobank.bcd.mapper.EmpnoMapper;
import com.kakaobank.bcd.service.EmpnoService;

import lombok.RequiredArgsConstructor;

@RestController
@RequiredArgsConstructor
public class EmpnoController {
	private final EmpnoService empnoService;
	
	@RequestMapping(value="/empno/select", method = RequestMethod.GET)
	public EmpnoDto selectByEmpno(EmpnoDto empInput) {
		
		empInput.setEmpno("200924");
		
		return empnoService.selectByEmpno(empInput);
	}
}
