import axios from 'axios'


export default class Message{

    protected axios
    
    public constructor(axiosConfig: {}){
        this.axios = axios.create(axiosConfig)
    }


    public async tran(method: string , url: string, parmas: {}){
        
        if(method == "get"){
            return await this.awaitGet(url);
        }
        else if(method == "post"){
            return await this.awaitPost(url, parmas);
        }

    }

    //promise라는 것으로 return 받음...!
    private async awaitGet(url: string){
        return await this.axios.get(url).then((response) => {return response.data}).catch((error) =>{ throw error.response})
    }

    private async awaitPost(url: string, params: {}){
        return await this.axios.post(url, params).then((response) => {
            return response.data}).catch((error) =>{ throw error.response})
    }
}