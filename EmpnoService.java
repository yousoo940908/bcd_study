package com.kakaobank.bcd.service;

import org.springframework.stereotype.Service;

import com.kakaobank.bcd.domain.EmpnoDto;
import com.kakaobank.bcd.mapper.EmpnoMapper;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
@RequiredArgsConstructor
public class EmpnoService {
	private final EmpnoMapper empnoMapper;
	
	public EmpnoDto selectByEmpno(EmpnoDto emp) {
		System.out.println("후후후후후후");
		EmpnoDto selectedEmpno = empnoMapper.selectbyEmpno(emp.getEmpno());
		
		System.out.println(">> selectedEmpno : " + selectedEmpno);
		
		return selectedEmpno;
	}

}
